import pprint
from finesse.script.spec import KatSpec

spec = KatSpec()

commands = [*spec.commands, *spec.analyses]
commands.sort(key = lambda item: (-len(item), item))

elements = list(spec.elements)
elements.sort(key = lambda item: (-len(item), item))

functions = list(spec.expression_functions)
functions.sort(key = lambda item: (-len(item), item))

keywords = list(spec.reserved_names)
keywords.sort(key = lambda item: (-len(item), item))

operators = list({*spec.binary_operators, *spec.unary_operators})
# "&" isn't really treated like an operator in KatScript, but highlight it as one
operators.append("&")
# The only multi-character operators are repetitions of the same character, so we remove them to
# allow square brackets in regex to work.
for x in [op for op in operators if len(op) > 1]:
    operators.remove(x)

# "-" has to appear at the end of the list, as all these operators will go into square brackets in
# a regex, and "-" in the middle of square brackets implies a range.
operators.remove("-")
operators.append("-")

with open("src/codemirror-katscript-types.ts", "w") as file:
    file.write(f"export const commands = ")
    pprint.pprint(commands, stream=file, indent=2)

    file.write(f"export const elements = ")
    pprint.pprint(elements, stream=file, indent=2)

    file.write(f"export const functions = ")
    pprint.pprint(functions, stream=file, indent=2)

    file.write(f"export const keywords = ")
    pprint.pprint(keywords, stream=file, indent=2)

    file.write(f"export const operators = ")
    pprint.pprint(operators, stream=file, indent=2)
