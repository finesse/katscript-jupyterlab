import { ICodeMirror } from '@jupyterlab/codemirror';

const components = [
  'bs[12]?',
  'dbs',
  'grd*',
  'isol',
  'l',
  'lens',
  'm[12]?',
  'mod',
  's',
  'sq',
];

const detectors = [
  'ad',
  'beam',
  'bp',
  'cp',
  'guoy',
  'hd',
  'pd[SN]?d*',
  'pgaind',
  'qd',
  'qd*hd?',
  'qhd[SN]?',
  'qnoised[SN]?',
  'qshot',
  'qshot[SN]?',
  'sd',
  'xd',
];

const commands = [
  'attr',
  'cav',
  'conf',
  'fadd',
  'fsig',
  'gauss',
  'knm',
  'lambda',
  'map',
  'mask',
  'maxtem',
  'pdtype',
  'phase',
  'retrace',
  'smotion',
  'startnode',
  'tem',
  'tf2?',
  'vacuum',

  // Plotting:
  'const',
  'deriv_h',
  'diff',
  'func',
  'lock',
  'noplot',
  'noxaxis',
  'put',
  'scale',
  'set',
  'trace',
  'var',
  'x2?axis',
  'yaxis',

  // Auxiliary
  'gnuterm',
  'multi',
  'pause',
  'pyterm',
];

const reserved = ['dump'];

function wordListRegexp(words: string[]): RegExp {
  return new RegExp('^((' + words.join(')|(') + '))$');
}

const regex = {
  command: wordListRegexp(commands),
  component: wordListRegexp(components),
  detector: wordListRegexp(detectors),
  name: /[a-zA-Z_][a-zA-Z0-9_-]/,
  number: /^(([+-]?inf)|([+-]?(\d+\.\d*|\d*\.\d+|\d+)([eE]-?\d*\.?\d*)?j?([pnumkMGT])?))$/,
  reserved: wordListRegexp(reserved),
  variable: /\$\w+/,
};

/*
 * Available tokens:
 *
 * keyword
 * atom
 * number
 * def
 * variable
 * variable-2
 * variable-3
 * property
 * operator
 * comment
 * string
 * string-2
 * meta
 * qualifier
 * builtin
 * bracket
 * tag
 * attribute
 * header
 * quote
 * hr
 * link
 */

export function defineKatScript2Mode(codemirror: ICodeMirror): void {
  const cm = codemirror.CodeMirror;
  cm.defineMode('katscript2', () => {
    function tokenBase(stream: CodeMirror.StringStream, state: any) {
      return 'line-background-katscript ' + tokenLex(stream, state);
    }

    function tokenLex(stream: CodeMirror.StringStream, state: any) {
      if (stream.sol()) {
        state.firstOnLine = true;
      }
      if (stream.eatSpace()) {
        return;
      }
      if (/[#%]/.test(stream.peek())) {
        stream.skipToEnd();
        return 'comment';
      }
      if (stream.peek() === '$') {
        stream.next();
      }
      stream.eatWhile(/[^\s]/);
      let style = null;
      const cur = stream.current();

      if (state.firstOnLine) {
        if (regex['component'].test(cur) || regex['detector'].test(cur)) {
          // Component definition
          style = 'keyword';
          state.nextIsDef = true;
        } else if (regex['command'].test(cur)) {
          // Command
          style = 'builtin';
        }
      } else if (state.nextIsDef && regex['name'].test(cur)) {
        // Component name
        style = 'def';
        state.nextIsDef = false;
      } else if (regex['number'].test(cur)) {
        // Number
        style = 'number';
      } else if (regex['reserved'].test(cur)) {
        // Reserved keyword
        style = 'keyword';
      } else if (regex['variable'].test(cur)) {
        style = 'variable-2';
      }
      if (style !== null) {
        state.firstOnLine = false;
        return style;
      }
      stream.next();
    }

    return {
      startState: function () {
        return {
          tokenize: tokenBase,
          firstOnLine: true,
          nextIsDef: false,
        };
      },
      blankLine: function (state: any) {
        return 'line-background-katscript';
      },
      token: function (stream: CodeMirror.StringStream, state: any) {
        return state.tokenize(stream, state);
      },
    };
  });

  cm.defineMIME('text/x-katscript2', 'katscript2');
}
