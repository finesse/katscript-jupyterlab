import { ICodeMirror } from '@jupyterlab/codemirror';

declare module 'codemirror' {
  function myMultiplexingMode(outer: any, ...others: any): any
}

export function defineMultiplexingMode(
codemirror: ICodeMirror,
): void
